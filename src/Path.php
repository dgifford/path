<?php
Namespace dgifford\Path;



/*
	Class for manipulating paths and files.

	If any method receives an array instead of a string as a path,
	the array is joined into a single string.
 */



class Path
{
	// The path string
	public $path;

	// The directory separator
	protected $ds;

	protected $pathinfo_options = 
	[
		'dirname' 	=> 'PATHINFO_DIRNAME',
		'basename' 	=> 'PATHINFO_BASENAME',
		'extension' => 'PATHINFO_EXTENSION',
		'filename' 	=> 'PATHINFO_FILENAME',
	];






	public function __construct()
	{
		$this->ds = DIRECTORY_SEPARATOR;

		if( func_num_args() > 0 )
		{
			call_user_func_array([ $this, 'set' ], func_get_args());
		}
	}



	/**
	 * Accepts a string or an array.
	 * An array is automatically joined into a string.
	 */
	public function set( $convert_slashes = true )
	{
		$args = func_get_args();

		// Single string argument
		if( func_num_args() == 1 and is_string($args[0]) )
		{
			$this->path = $args[0];
		}
		elseif( func_num_args() > 1 or is_array($args[0]))
		{
			$this->join( $args );
		}
		else
		{
			throw new \InvalidArgumentException("Invalid path.");
		}

		if( $convert_slashes )
		{
			$this->path = str_replace(['/','\\', $this->ds ], $this->ds, $this->path);
		}

		// Chainable
		return $this;
	}



	/**
	 * Returns the path.
	 * 
	 * @return string Path
	 */
	public function get()
	{
		return $this->path;
	}






	///////////////////////////////////////////////
	// static_shared methods are shared between
	// static and non-static calls
	///////////////////////////////////////////////



	
	///////////////////////////////////////////////
	// Conversion methods
	///////////////////////////////////////////////



	/**
	 * Sets the path by joining an array of strings together.
	 * 
	 * @return string 	Path
	 */
	public function join_static_shared()
	{
		if( func_num_args() > 0 )
		{
			$result = [];
			$args = func_get_args();

			// Flatten array
			array_walk_recursive($args, function($a) use (&$result) { $result[] = $a; }); 

			// Trim parts
			foreach( $result as $index => &$part )
			{
				if( $index == 0 )
				{
					$part = rtrim( $part, '\\/');
				}
				else
				{
					$part = trim( $part, '\\/');
				}
			}

			$this->set( implode( $this->ds, $result) );
		}

		// Chainable
		return $this;
	}



	/**
	 * Convert the path into a real path or sets path to boolean false 
	 * if it doesn't exist.
	 * 
	 * @return $this 	Chainable
	 */
	protected function real_static_shared()
	{
		$this->path = realpath( $this->path );

		// Chainable
		return $this;
	}





	///////////////////////////////////////////////
	// Boolean
	///////////////////////////////////////////////



	/**
	 * Returns false if the path doesn't exist.
	 * 
	 * @return bool
	 */
	protected function exists_static_shared()
	{
		if( realpath( $this->path ) === false )
		{
			return false;
		}

		return true;
	}



	/**
	 * Returns false if the path isn't writeable.
	 * 
	 * @return bool
	 */
	protected function writeable_static_shared()
	{
		return is_writable( $this->path );
	}



	/**
	 * Returns true if the path contains the sub path string.
	 * 
	 * @param  string $sub_path
	 * @return bool
	 */
	public function contains( $sub_path = '' )
	{
		if( !empty($sub_path) and strpos( $this->path, $sub_path ) !== false )
		{
			return true;
		}

		return false;
	}



	/**
	 * Check if a given path is a subpath of the path.
	 * 
	 * @param  string  $subpath
	 * @return boolean
	 */
	public function hasSubPath( $subpath )
	{
		return $this->subPath( $subpath, $this->path );
	}



	/**
	 * Check if the object path is a subpath of the given path.
	 * 
	 * @param  string  $path
	 * @return boolean
	 */
	public function isSubPath( $path )
	{
		return $this->subPath( $this->path, $path );
	}



	/**
	 * Determines if one path is a subpath (lower level) of another.
	 * @param  string $subpath
	 * @param  string $path
	 * @return boolean
	 */
	protected function subPath( $subpath, $path )
	{
		$subpath = realpath($subpath);
		$path = realpath($path);

		if( $subpath and $path and (strpos( $subpath, $path ) === 0 and $subpath !== $path) )
		{
			return true;
		}

		return false;
	}



	/**
	 * From symphony filesystem component
	 * http://symfony.com/doc/current/components/filesystem.html
	 * 
	 * @return bool
	 */
	public function isAbsolute_static_shared()
	{
		return strspn( $this->path, '/\\', 0, 1)
			|| (strlen($this->path) > 3 && ctype_alpha($this->path[0])
				&& substr($this->path, 1, 1) === ':'
				&& strspn($this->path, '/\\', 2, 1)
			)
			|| null !== parse_url($this->path, PHP_URL_SCHEME)
		;
	}



	public function isRelative_static_shared()
	{
		return !$this->isAbsolute();
	}






	///////////////////////////////////////////////
	// Path information
	///////////////////////////////////////////////



	public function length_static_shared()
	{
		return strlen( $this->path );
	}


	/**
	 * Gets info about the path
	 * 
	 * @param  string $option 	path_info result key
	 * @return string/ array 	String if option provided, otherwise an array
	 */
	protected function info_static_shared( $option = '' )
	{
		if( isset( $this->pathinfo_options[ $option ]) )
		{
			return pathinfo( $this->path, constant( $this->pathinfo_options[ $option ] ) );
		}

		return pathinfo( $this->path );
	}



	protected function dirname_static_shared()
	{
		return $this->info( 'dirname' );
	}



	public function basename_static_shared()
	{
		return $this->info( 'basename' );
	}



	protected function extension_static_shared()
	{
		return $this->info( 'extension' );
	}



	protected function filename_static_shared()
	{
		return $this->info( 'filename' );
	}









	////////////////////////////////////////////
	// Overloaders - allow the use of the same 
	// method names for static and non-static
	////////////////////////////////////////////



	public function __call( $name, $args = [] )
	{
		$name = Path::getMethodName( $name );

		if( !empty( $args ) )
		{
			return call_user_func_array([ $this, $name], $args );
		}
		else
		{
			return $this->$name();
		}
	}



	/**
	 * When called statically, multiple paths must be
	 * contained in an array.
	 * 
	 * @param  [type] $name [description]
	 * @param  [type] $args [description]
	 * @return [type]       [description]
	 */
	public static function __callStatic( $name, $args )
	{
		$name = Path::getMethodName( $name );

		// Error if no arguments
		if( count($args) == 0 )
		{
			throw new \InvalidArgumentException("Invalid arguments for Path::static.");
		}
		
		// Create path object
		$path = new Path;

		call_user_func_array([$path, 'set'], $args );

		$result = call_user_func([$path, $name]);

		if( $result instanceof Path )
		{
			return $result->get();
		}
		else
		{
			return $result;
		}
	}



	/**
	 * Checks that the method exists.
	 * 
	 * @param  string $name
	 * @return bool
	 */
	protected static function getMethodName( $name = '' )
	{
		if( !method_exists( (new Path), $name . '_static_shared' ) )
		{
			throw new \BadMethodCallException("Method '{$name}' does not exist." );			
		}

		return $name . '_static_shared';
	}
}