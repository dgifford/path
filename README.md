# Path #

A class for manipulating paths.

### Usage ###

The class can be used non-statically:

```
$path = new Path( '/path/to/a/file.ext' );

// Boolean check if the path exists
$path->exists();

// Boolean check if the path is writeable
$path->writeable();

// Get components of the path
$path->extension(); // returns 'ext'

// Get the path
$path->get(); // Returns '/path/to/a/file.ext'

// Change the path
$path->set('path/to/a/foo/../file.ext' );

// Resolve the path to a real path
$path->real()->get(); // Returns '/path/to/a/file.ext' or false if it doesn't exist

```

And statically:
```
// Boolean check if a path exists
Path::exists( '/path/to/a/file.ext' );

// Boolean check if a path is writeable
Path::writeable( '/path/to/a/file.ext' );

// Get components of the path
Path::extension( '/path/to/a/file.ext' ); // returns 'ext'

// Resolve a path to a real path
Path::real('path/to/a/foo/../file.ext' ); // Returns '/path/to/a/file.ext' or false if it doesn't exist
```


The class can also accept multiple arguments:
```
// Join strings to make a path
$path = new Path( '/path/', '/to/', '\a\', 'file.ext' );

$path->join()->get(); // Returns '/path/to/a/file.ext'

// Boolean check if any of the paths don't exist
$path->set( '/path/to/a/file.ext', 'foo bar', '/var/etc/' );

$path->exists(); // Returns false

// Boolean check if each of the paths exists
$path->exists( true ); // Returns [ true, false, true ]
```