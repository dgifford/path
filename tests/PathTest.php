<?php
Namespace dgifford\Path;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class PathTest extends \PHPUnit_Framework_TestCase
{
	public function testConstructor()
	{
		$path = new Path( __DIR__ );

		$this->assertTrue( $path->exists() );

		$this->assertTrue( $path->writeable() );

		$this->assertTrue( $path->contains( '/path/tests' ) );

		$this->assertFalse( $path->hasSubPath( __DIR__ . '/../' ) );

		$this->assertTrue( $path->isAbsolute() );

		$this->assertFalse( $path->isRelative() );
	}



	public function testSet()
	{
		$path = new Path;

		$path->set( __DIR__ );

		$this->assertTrue( $path->exists() );

		$this->assertTrue( $path->writeable() );

		$this->assertTrue( $path->contains( '/path/tests' ) );

		$this->assertFalse( $path->hasSubPath( __DIR__ . '/../' ) );

		$this->assertTrue( $path->isAbsolute() );
		
		$this->assertFalse( $path->isRelative() );
	}



	public function testSetWithMixedSlashes()
	{
		$path = new Path;

		$path->set( 'path/to/a', '\\foo\\bar\\', 'filename.ext' );

		$this->assertSame( 'path/to/a/foo/bar/filename.ext', $path->get() );
	}



    /**
     * @expectedException     \InvalidArgumentException
     */
	public function testSetInvalidArgument()
	{
		$path = new Path;

		$path->set( false );
	}



	public function testSetWithList()
	{
		$path = new Path( __DIR__, '/../..' );

		$this->assertSame( __DIR__ . '/../..', $path->get() );

		$this->assertTrue( $path->exists() );

		$this->assertTrue( $path->writeable() );

		$this->assertTrue( $path->hasSubPath( __DIR__ . '/../' ) );

		$this->assertTrue( $path->isAbsolute() );
		
		$this->assertFalse( $path->isRelative() );
	}



	public function testSetWithArray()
	{
		$path = new Path([ __DIR__, '/../..' ]);

		$this->assertSame( __DIR__ . '/../..', $path->get() );

		$this->assertTrue( $path->exists() );

		$this->assertTrue( $path->writeable() );

		$this->assertTrue( $path->hasSubPath( __DIR__ . '/../' ) );

		$this->assertTrue( $path->isAbsolute() );
		
		$this->assertFalse( $path->isRelative() );
	}



	public function testPathExists()
	{
		$path = new Path( 'path/one' );

		$this->assertFalse( $path->exists() );

		$path->set( __DIR__ );

		$this->assertTrue( $path->exists() );

		$path->set( __DIR__, 'path/one', 'path/two' );

		$this->assertFalse( $path->exists() );
	}



	public function testPathIsReal()
	{
		$path = new Path( 'path/one' );

		$this->assertFalse( $path->real()->get() );

		$path->set( __DIR__ );

		$this->assertSame( __DIR__, $path->real()->get() );

		$path->set( __DIR__, '/../src' );

		$this->assertSame( realpath(__DIR__ .'/../src'), $path->real()->get() );
	}



	public function testJoinPaths()
	{
		$path = new Path;

		$this->assertSame( 'path/one/more/filename.txt', $path->join( 'path/one', 'more', 'filename.txt' )->get() );

		$this->assertFalse( $path->exists() );

		$path->set( __DIR__ . '/../src', 'Path.php' );

		$this->assertTrue( $path->exists() );

		$this->assertTrue( $path->writeable() );
	}



	public function testInfo()
	{
		$path = new Path( __DIR__ . '/../src/Path.php' );

		$this->assertTrue( $path->real()->exists() );

		$this->assertSame( 'php', $path->extension() );
		
		$this->assertSame( 'Path', $path->filename() );
	}



	public function testhasSubPath()
	{
		$path = new Path( __DIR__ . '/..' );

		$this->assertTrue( $path->hasSubPath( __DIR__ ) );

		$this->assertFalse( $path->hasSubPath( __DIR__ . '/..' ) );
	}



	public function testStaticExists()
	{
		$this->assertTrue( Path::exists( __DIR__ . '/../src/Path.php' ) );
	}



	public function testStaticReal()
	{
		$this->assertSame( realpath(__DIR__ .'/../src'), Path::real( __DIR__ . '/../src' ) );

	}



	public function testStaticJoin()
	{
		$this->assertSame( 'path/one/more/filename.txt', Path::join('path/one', 'more', 'filename.txt') );
	}



	public function testStaticWriteable()
	{
		$this->assertFalse( Path::writeable([ __DIR__, 'more', 'filename.txt' ]) );
	}



	public function testStaticExtension()
	{
		$this->assertSame( 'php', Path::extension( __DIR__ . '/../src/Path.php' ) );
	}



	public function testStaticGetFilename()
	{
		$this->assertSame( 'PathTest', Path::filename( __FILE__ ) );
	}



	public function testStaticIsAbsolute()
	{
		$this->assertTrue( Path::isAbsolute( __FILE__ ) );

		$this->assertFalse( Path::isRelative( __FILE__ ) );
	}



	public function testStaticIsRelative()
	{
		$this->assertTrue( Path::isRelative( 'my/path/filename.ext' ) );

		$this->assertFalse( Path::isAbsolute( 'my/path/filename.ext' ) );
	}



    /**
     * @expectedException     \BadMethodCallException
     */
	public function testMethodDoesntExist()
	{
		$path = new Path;

		$path->foobar();
	}



    /**
     * @expectedException     \BadMethodCallException
     */
	public function testStaticMethodDoesntExist()
	{
		Path::foobar('wibble');
	}
}